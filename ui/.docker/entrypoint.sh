#!/bin/bash

cd /var/www

# Correct ownership of the node_modules folder (Reason: The mount in docker-compose forces it to root)
mkdir -p node_modules
sudo chown -R asics:asics node_modules
npm i

exec npm run serve