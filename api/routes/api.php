<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// The api route to the matrix calculation, also enforce json header here.
Route::middleware(["enforce.json"])->post(
    '/calculate',
    'App\Http\Controllers\MatrixController@calculate'
);
