<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\MatrixCalculationRequest;
use App\Services\MatrixService;
use Illuminate\Http\JsonResponse;

class MatrixController extends Controller
{
    /**
     * Multiply two matrices and return an alphabetic response matrix.
     *
     * @param MatrixCalculationRequest $request
     * @param MatrixService $service
     *
     * @return JsonResponse
     */
    public function calculate(MatrixCalculationRequest $request, MatrixService $service): JsonResponse
    {
        $validated = $request->validated();

        // extra backend validation should be done here (calculating the columns in matrix 1 and rows in matrix 2)

        $result = $service->calculate(
            $validated['matrix1'],
            $validated['matrix2']
        );

        return response()->json([
            'message' => 'Success',
            'data' => $result
        ]);
    }
}
