<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MatrixCalculationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'matrix1'       => ['required', 'array'],
            'matrix1.*'     => ['required', 'array'],
            'matrix1.*.*'   => ['required', 'integer'],
            'matrix2'       => ['required', 'array'],
            'matrix2.*'     => ['required', 'array'],
            'matrix2.*.*'   => ['required', 'integer'],
        ];
    }
}
