<?php

declare(strict_types=1);

namespace App\Services;

use SciPhp\NumPhp as NumPhp;

class MatrixService
{
    /**
     * Multiply two matrices and return result.
     * for easier calculations we make use of the NumPhp library
     *
     * @param array   $matrix1
     * @param array   $matrix2
     *
     * @return array
     */
    public function calculate(array $matrix1, array $matrix2): array
    {
        $result = [];

        $matrix = NumPhp::ar($matrix1)->dot($matrix2)->data;

        foreach ($matrix as $index => $values) {
           foreach($values as $value) {
               $result[$index][] = $this->toStr($value);
           }
        }

        return $result;
    }

    /**
     * Convert number to string.
     * example: 1 > A, 26 > Z, 27 > AA etc.
     *
     * @param int $num
     *
     * @return string
     */
    function toStr($num): string
    {
        if ($num <= 0) {
            return config('matrix.placeholder');
        }

        $str = '';
        while ($num > 0) {
            $num--;
            $str = chr($num % 26 + 65) . $str;
            $num = (int) ($num / 26);
        }

        return $str;
    }
}
