<?php

return [

    /*
     * Placeholder for when a matrix calculation fails.
     */
    'placeholder' => "-"
];
