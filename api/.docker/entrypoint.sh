#!/bin/bash

cd /var/www

# Correct ownership of the vendor folder (Reason: The mount in docker-compose forces it to root)
sudo chown -R asics:asics vendor
sudo chown -R asics:asics storage

composer install -q

#exec /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
