<?php

namespace Tests\Unit;

use App\Services\MatrixService;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\CreatesApplication;

class MatrixServiceTest extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Test for multiply() method of MatrixService.
     */
    public function testMultiply()
    {
        $matrixService = new MatrixService;

        $matrix1 = [[0,0],[0,0]];
        $matrix2 = [[0,0],[0,0]];
        $expecting = [
            [config('matrix.placeholder'),config('matrix.placeholder')],
            [config('matrix.placeholder'),config('matrix.placeholder')],
        ];

        $this->assertEquals($expecting, $matrixService->calculate($matrix1, $matrix2));

        $matrix1 = [[1,2],[3,4]];
        $matrix2 = [[4,3],[2,1]];
        $expecting = [['H','E'],['T','M']];

        $this->assertEquals($expecting, $matrixService->calculate($matrix1, $matrix2));

        $matrix1 = [[3,5],[4,6]];
        $matrix2 = [[9,8],[7,6]];
        $expecting = [
            [$matrixService->toStr(62), $matrixService->toStr(54)],
            [$matrixService->toStr(78), $matrixService->toStr(68)],
        ];

        $this->assertEquals($expecting, $matrixService->calculate($matrix1, $matrix2));
    }

    /**
     * Test for toStr() method of MatrixService.
     */
    public function testToStr()
    {
        $matrixService = new MatrixService;

        $this->assertEquals(config('matrix.placeholder'), $matrixService->toStr(0));
        $this->assertEquals('A', $matrixService->toStr(1));
        $this->assertEquals('B', $matrixService->toStr(2));
        $this->assertEquals('Z', $matrixService->toStr(26));
        $this->assertEquals('AA', $matrixService->toStr(27));
        $this->assertEquals('AB', $matrixService->toStr(28));
    }
}
