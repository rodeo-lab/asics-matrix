# Asics Matrix Multiplication
This repository contains the assessment from Asics.

## Dependencies
* Docker
* Docker compose
* Make

## Starting up
The project is based on two separate docker containers which can be run easily with docker compose. To do so, follow the next steps:

- Open a terminal in the root of the project and use the next commands:
- `make build`
- `make up`
- `make install` 
- Visit the url: http://localhost:8051/

## Running tests
After the startup you can run the php unit tests with `make test`

## Make commands
```bash
Usage:  make COMMAND

Commands:
help:    Show this help
build:   (Re)build locally the docker images for this application
down:    Stop and remove the docker containers for local development
stop:    Stop the docker containers for local development
tail:    Tail the log files of the containers
up:      Start the docker containers for local development
shell:   Open a bash shell to the PHP container.
test:    Runs the phpunit tests.
```
